<?php
require 'TelegraphText.php';

$author = 'Егор Летов';
$slug = 'letov.txt';
$title = 'Lorem ipsum dolor sit amet.';
$text = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse, optio asperiores! Eos, temporibus facilis. Dolorum a, dolorem quia ex dolores officia. Enim eos molestiae corrupti et voluptatem atque quidem provident.';

$newText = new TelegraphText($author, $slug);
$newText->editText($title, $text);
$newText->storeText();
var_dump($newText->loadText());