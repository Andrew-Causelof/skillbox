<?php
class TelegraphText 
{
    protected $title;     // заголовок текста
    protected $text;      // текст
    protected $author;    // имя автора;
    protected $published; //дата и время последнего изменения текста
    protected $slug;      //уникальное имя для файла

    public function __construct($author,  $slug)
    {
       $this->author    = $author;
       $this->slug      = $slug;
       $this->published = (new DateTime())->getTimestamp(); 
    }

    public function storeText()
    {
       return file_put_contents($this->slug,
                                serialize([
                                    'text'      => $this->text,
                                    'title'     => $this->title,
                                    'author'    => $this->author,
                                    'published' => $this->published
                                ])
                                );
     
    }

    public function loadText()
    {
        $content = file_get_contents($this->slug);
        if(!empty($content)) {
            $datas = unserialize($content);
            $this->title     = $datas['title'];
            $this->text      = $datas['text'];
            $this->author    = $datas['author'];
            $this->published = $datas['published'];

            return $this->text;
        } else {
            return false;
        }
    }

    public function editText(string $title, string $text)
    {
        $this->title = $title;
        $this->text  = $text;
    }

}
